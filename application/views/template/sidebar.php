<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->
            <a href="<?php echo base_url();?>" class="sidebar-brand">
                Kawal C1 - DA1
            </a>
            <!-- END Brand -->

            <!-- Sidebar Navigation -->
            <div class="sidebar-header sidebar-nav-mini-hide">
                <span class="sidebar-header-title">Menu</span>
            </div>

            <ul class="sidebar-nav">
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-file sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Form C1</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_c_satu"><span class="sidebar-nav-mini-hide">Masukkan C1</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_c_satu/lapor_salah"><span class="sidebar-nav-mini-hide">Lapor Kesalahan KPU</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_c_satu/lapor_janggal"><span class="sidebar-nav-mini-hide">Lapor Kejanggalan KPU</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-list sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Tabulasi Form C1</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_c_satu/rekap_form_c1"><span class="sidebar-nav-mini-hide">Form C1</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_c_satu/rekap_lapor_salah"><span class="sidebar-nav-mini-hide">Kesalahan KPU</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_c_satu/rekap_lapor_janggal"><span class="sidebar-nav-mini-hide">Kejanggalan KPU</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-file sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Form DA1</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_da_satu"><span class="sidebar-nav-mini-hide">Masukkan DA1</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_da_satu/lapor_salah"><span class="sidebar-nav-mini-hide">Lapor Kesalahan KPU</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_da_satu/lapor_janggal"><span class="sidebar-nav-mini-hide">Lapor Kejanggalan KPU</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-list sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Tabulasi Form DA1</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_da_satu/rekap_form_da1"><span class="sidebar-nav-mini-hide">Form DA1</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_da_satu/rekap_lapor_salah"><span class="sidebar-nav-mini-hide">Kesalahan KPU</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/form_da_satu/rekap_lapor_janggal"><span class="sidebar-nav-mini-hide">Kejanggalan KPU</span></a>
                        </li>
                    </ul>
                </li>
                <!--
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-list sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Daftar form C1 Pembanding</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>index.php/pembanding"><span class="sidebar-nav-mini-hide">Laporan Ketidaksesuaian</span></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>index.php/pembanding/daftar_pembanding_janggal"><span class="sidebar-nav-mini-hide">Laporan Kejanggalan</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-file sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">form DA1</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>index.php/tambah"><span class="sidebar-nav-mini-hide">Input</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-list sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Daftar form DA1</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>index.php/daftar/daftar_form"><span class="sidebar-nav-mini-hide">Daftar</span></a>
                        </li>
                    </ul>
                </li>
                -->
            </ul>
            <!-- END Sidebar Navigation -->
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>
