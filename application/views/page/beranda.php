<div class="block full block-alt-noborder">
    <h3 class="sub-header">Data Karyawan</h3>

    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="table-responsive">
                <table cellspacing="5" cellpadding="5" border="0">
                    <tbody>
                        <tr>
                            <td>Minimum date:</td>
                            <td><input type="text" id="min" name="min"></td>
                        </tr>
                        <tr>
                            <td>Maximum date:</td>
                            <td><input type="text" id="max" name="max"></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <table id="example" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>Employee ID</th>
                            <th>Employee Name</th>
                            <th>Employee Email</th>
                            <th>Employee Phone</th>
                            <th>Office</th>
                            <th>Order Date</th>
                            <th>Order Item</th>
                            <th>Order Amount</th>
                            <th>Client ID</th>
                            <th>Client Name</th>
                            <th>Client Email</th>
                            <th>Client Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($rekap_data != ""){
                                foreach($rekap_data as $row):
                        ?>
                        <tr>
                            <td><?php echo($row->employee_id);?></td>
                            <td><?php echo($row->name);?></td>
                            <td><?php echo($row->email);?></td>
                            <td><?php echo($row->phone);?></td>
                            <td><?php echo($row->office);?></td>
                            <td><?php echo($row->date_order);?></td>
                            <td><?php echo($row->item_order);?></td>
                            <td><?php echo($row->amount);?></td>
                            <td><?php echo($row->client_id);?></td>
                            <td><?php echo($row->client_name);?></td>
                            <td><?php echo($row->client_email);?></td>
                            <td><?php echo($row->client_phone);?></td>
                        </tr>
                        <?php 
                                endforeach;
                            } 
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
