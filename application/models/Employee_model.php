<?php

Class Employee_model extends CI_Model {

	/*======================== MODEL GLOBAL =========================================*/


	public function __construct(){
		
		$this->load->database();
		
	}

	function ambil_data_employee(){

		$this->db->order_by('id','ASC');
		$query=$getData=$this->db->get('employee');
		if($getData->num_rows()>0){
			return $query->result();
		}else{
			return null;
		}

	}

}

?>